﻿using System;
using System.IO;
using LuckyTickets;
using NUnit.Framework;

namespace LuckyTicketsTests
{
    [TestFixture]
    public class Tests
    {
        private const string TestsPath = @"C:\Users\Пользователь\RiderProjects\OtusAlgorythms\TestControlResults\A01_LuckyTickets\1.Tickets\";

        [Test]
        public void CheckLuckyNumbersAlgorithm()
        {
            var nr = 0;
            var luckyTicketsCounter = new LuckyTicketsCounter();
            bool allAreRight = true;
            while (true)
            {
                var inFile = $"{TestsPath}test.{nr}.in";
                var outFile = $"{TestsPath}test.{nr}.out";
                if (!File.Exists(inFile) || !File.Exists(outFile))
                {
                    break;
                }
                Console.WriteLine($"Test #{nr}:");
                allAreRight = RunTest(inFile, outFile, luckyTicketsCounter);
                nr++;
                Console.WriteLine();
            }

            if (nr == 0)
            {                    
                Console.WriteLine("Не удалось получить тестовые файлы.");
                allAreRight = false;
            }
            Assert.True(allAreRight);
        }

        private bool RunTest(string inFile, string outFile, ILuckyTicketsCounter luckyTicketsCounter)
        {
            try
            {
                var inData = File.ReadAllText(inFile);
                if (!int.TryParse(inData, out var countOfHalfTicketNumbers))
                    return false;
                var expect = File.ReadAllText(outFile).Trim();
                var actual = luckyTicketsCounter.GetCountLuckyTickets(countOfHalfTicketNumbers).ToString();
                Console.WriteLine($"In: {inData}\n" +
                                  $"Expect: {expect}\n" +
                                  $"Out: {actual}");
                return expect.Equals(actual);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Ошибка при выполнении теста: {e.Message}");
                return false;
            }
        }
    }
}