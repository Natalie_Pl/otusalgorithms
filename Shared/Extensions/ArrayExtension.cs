﻿using System;

namespace Shared.Extensions
{
    public static class ArrayExtension
    {
        public static void Print(this int[,] array)
        {
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    Console.Write(array[x, y] + " ");
                }

                Console.WriteLine();
            }
        }
    }
}