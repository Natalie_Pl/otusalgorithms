﻿using System.Collections.Generic;
using System.Linq;
using Shared.Extensions;

namespace LuckyTickets
{
    public class LuckyTicketsCounter : ILuckyTicketsCounter
    {
        /// <summary>
        /// Возвращает количество всех счастливых билетов.
        /// </summary>
        /// <param name="symbolsCount">Количество цифр половины билета.</param>
        public long GetCountLuckyTickets(int symbolsCount)
        {
            const int numbersCount = 10;
            
            // Максимальная сумма чисел половины номера билета.
            var maxSum = 9 * symbolsCount + 1;

            // Список вариаций количества билетов в одной строке.
            var countOfTicketsOneLine = new List<int>() { 1 };
            var array = new int[numbersCount, maxSum];

            for (var i = 1; i <= symbolsCount; i++)
            {
                countOfTicketsOneLine = GetCountOfLuckyNumbers(array, countOfTicketsOneLine);
                
                // Печать массива для анализа
                // array.Print();
            }

            return GetSumOfSquares(countOfTicketsOneLine.Select(c=> (long)c).ToList());
        }

        private static long GetSumOfSquares(List<long> countOfTickets)
        {
            long result = 0;
            countOfTickets.ForEach(c =>
                result += c * c);
            return result;
        }

        private static List<int> GetCountOfLuckyNumbers(int[,] array, List<int> countsOfTickets)
        {
            for (var number = 0; number < array.GetLength(0); number++)
            {
                for (var sum = 0; sum < array.GetLength(1); sum++)
                {
                    if (number == sum)
                    {
                        AddCountOfTickets(array, number, sum, countsOfTickets);
                        break;
                    }

                    array[number, sum] = 0;
                }
            }

            return GetFinalCountOfTicketsInArray(array);
        }

        private static List<int> GetFinalCountOfTicketsInArray(int[,] array)
        {
            var resLst = new List<int>();
            for (var sum = 0; sum < array.GetLength(1); sum++)
            {
                var sumInColumn = 0;
                for (var number = 0; number < array.GetLength(0); number++)
                {
                    sumInColumn += array[number, sum];
                }
                if (sumInColumn != 0)
                    resLst.Add(sumInColumn);
            }

            return resLst;
        }

        private static void AddCountOfTickets(int[,] array, int number, int sum, List<int> countsOfTickets)
        {
            foreach (var count in countsOfTickets)
            {
                array[number, sum] = count;
                sum++;
            }

            for (var i = sum; i < array.GetLength(1); i++)
            {
                array[number, i] = 0;
            }
        }
    }
}