﻿using System;
using System.Collections.Generic;

namespace LuckyTickets
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var luckyTicketsCounter = new LuckyTicketsCounter();
            var result = luckyTicketsCounter.GetCountLuckyTickets(4);
            Console.WriteLine(result);
        }

        /// <summary>
        /// Возвращает количество всех счастливых билетов.
        /// </summary>
        /// <param name="symbolsCount">Количество цифр половины билета.</param>
        private static long GetCountLuckyTickets(int symbolsCount)
        {
            const int numbersCount = 10;
            
            // Максимальная сумма чисел половины номера билета.
            var maxSum = 9 * symbolsCount + 1;

            // Список вариаций количества билетов в одной строке.
            var countOfTicketsOneLine = new List<int>() { 1 };
            var array = new int[numbersCount, maxSum];

            for (var i = 1; i <= symbolsCount; i++)
            {
                countOfTicketsOneLine = GetCountOfLuckyNumbers(array, countOfTicketsOneLine);
                PrintArray(array);
            }

            return GetSumOfSquares(countOfTicketsOneLine);
        }

        private static long GetSumOfSquares(List<int> countOfTickets)
        {
            long result = 0;
            countOfTickets.ForEach(c =>
                result += c * c);
            return result;
        }

        private static List<int> GetCountOfLuckyNumbers(int[,] array, List<int> countsOfTickets)
        {
            for (int number = 0; number < array.GetLength(0); number++)
            {
                for (int sum = 0; sum < array.GetLength(1); sum++)
                {
                    if (number == sum)
                    {
                        AddCountOfTickets(array, number, sum, countsOfTickets);
                        break;
                    }

                    array[number, sum] = 0;
                }
            }

            return GetFinalCountOfTicketsInArray(array);
        }

        private static List<int> GetFinalCountOfTicketsInArray(int[,] array)
        {
            var resLst = new List<int>();
            for (int sum = 0; sum < array.GetLength(1); sum++)
            {
                int sumInStolbec = 0;
                for (int number = 0; number < array.GetLength(0); number++)
                {
                    sumInStolbec += array[number, sum];
                }
                if (sumInStolbec != 0)
                    resLst.Add(sumInStolbec);
            }

            return resLst;
        }

        private static void AddCountOfTickets(int[,] array, int number, int sum, List<int> countsOfTickets)
        {
            foreach (var count in countsOfTickets)
            {
                array[number, sum] = count;
                sum++;
            }

            for (var i = sum; i < array.GetLength(1); i++)
            {
                array[number, i] = 0;
            }
        }

        private static void PrintArray(int[,] array)
        {
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    Console.Write(array[x, y] + " ");
                }

                Console.WriteLine();
            }
        }
    }
}