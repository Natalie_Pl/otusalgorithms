﻿namespace LuckyTickets
{
    /// <summary>
    /// Сервис подсчета количества счастливых билетов.
    /// </summary>
    public interface ILuckyTicketsCounter
    {
        /// <summary>
        /// Возвращает количество всех счастливых билетов.
        /// </summary>
        /// <param name="symbolsCount">Количество цифр половины билета.</param>
        long GetCountLuckyTickets(int symbolsCount);
    }
}