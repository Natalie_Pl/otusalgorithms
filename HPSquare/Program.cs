﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HPSquare
{
    internal class Program
    {
         private const string SpellName = "Заклинание";
        public static void Main()
        {
            Console.WriteLine($"{SpellName} 1");
            PrintSquare(Spell01);
            
            Console.WriteLine($"{SpellName} 2");
            PrintSquare(Spell02);
            
            Console.WriteLine($"{SpellName} 3");
            PrintSquare(Spell03);
            
            Console.WriteLine($"{SpellName} 4");
            PrintSquare(Spell04);
            
            Console.WriteLine($"{SpellName} 5");
            PrintSquare(Spell05);
            
            Console.WriteLine($"{SpellName} 6");
            PrintSquare(Spell06);
            
            Console.WriteLine($"{SpellName} 7");
            PrintSquare(Spell07);
            
            Console.WriteLine($"{SpellName} 8");
            PrintSquare(Spell08);
            
            Console.WriteLine($"{SpellName} 9");
            PrintSquare(Spell09);
            
            Console.WriteLine($"{SpellName} 10");
            PrintSquare(Spell10);
            
            Console.WriteLine($"{SpellName} 11");
            PrintSquare(Spell11);
            
            Console.WriteLine($"{SpellName} 12");
            PrintSquare(Spell12);
            
            Console.WriteLine($"{SpellName} 13");
            PrintSquare(Spell13);
            
            Console.WriteLine($"{SpellName} 14");
            PrintSquare(Spell14);
            
            Console.WriteLine($"{SpellName} 15");
            PrintSquare(Spell15);
            
            Console.WriteLine($"{SpellName} 16");
            PrintSquare(Spell16);
            
            Console.WriteLine($"{SpellName} 17");
            PrintSquare(Spell17);
            
            Console.WriteLine($"{SpellName} 18");
            PrintSquare(Spell18);
            
            Console.WriteLine($"{SpellName} 19");
            PrintSquare(Spell19);
            
            Console.WriteLine($"{SpellName} 20");
            PrintSquare(Spell20);
            
            Console.WriteLine($"{SpellName} 21");
            PrintSquare(Spell21);
            
            Console.WriteLine($"{SpellName} 22");
            PrintSquare(Spell22);
            
            Console.WriteLine($"{SpellName} 23");
            PrintSquare(Spell23);
            
            Console.WriteLine($"{SpellName} 24");
            PrintSquare(Spell24);
            
            Console.WriteLine($"{SpellName} 25");
            PrintSquare(Spell25);
        }

        private static void PrintSquare(Func<int, int, bool> spell)
        {
            for (var x = 0; x < 25; x++)
            {
                for (var y = 0; y < 25; y++)
                {
                    var symbol = spell(x , y)
                        ? "#  "
                        : ".  ";
                    Console.Write(symbol);
                }
                Console.WriteLine();
            }
        }
            
        private static bool Spell01 (int x, int y)
        {
            return y > x;
        }
        
        private static bool Spell02 (int x, int y)
        {
            return y == x;
        }
        
        private static bool Spell03 (int x, int y)
        {
            return y + x == 24;
        }
        
        private static bool Spell04 (int x, int y)
        {
            return y + x < 30;
        }
        
        private static bool Spell05 (int x, int y)
        {
            return x == (y - 1) / 2 || x == y / 2;
        }
        
        private static bool Spell06 (int x, int y)
        {
            return x < 10 || y < 10;
        }
        
        private static bool Spell07 (int x, int y)
        {
            return x > 15 && y > 15;
        }
        
        private static bool Spell08 (int x, int y)
        {
            return x * y == 0;
        }
        
        private static bool Spell09 (int x, int y)
        {
            return Math.Abs(x - y) > 10;
        }
        
        private static bool Spell10 (int x, int y)
        {
            return 2 * x - y + 1 >= 0 && x - y < 0;
        }
        
        private static bool Spell11 (int x, int y)
        {
            return new List<int> { x, y }.Any(e => e == 1 || e == 23);
            
            // Второй вариант записи
            //return x == 23 || x == 1 || y == 23 || y == 1;
        }
        
        private static bool Spell12 (int x, int y)
        {
            return x * x + y * y <= 20 * 20;
        }
        
        private static bool Spell13 (int x, int y)
        {
            return y + x >= 20 && y + x <= 28;
        }
        
        private static bool Spell14 (int x, int y)
        {
            return Math.Pow(x-25, 2) + Math.Pow(y-25, 2) >= Math.Pow(20, 2);
        }
        
        private static bool Spell15 (int x, int y)
        {
            return !(Math.Abs(x - y) > 20 || Math.Abs(x - y) < 10);
        }
        
        private static bool Spell16 (int x, int y)
        {
            return Math.Abs(x - 12) + Math.Abs(y - 12) < 10;
        }
        
        private static bool Spell17 (int x, int y)
        {
            return x >= Math.Cos(0.32 * (y - 4)) * 8 + 16;
        }
        
        private static bool Spell18 (int x, int y)
        {
            return new List<int> { x, y }.Any(e => e == 1 || e == 0) && !(x == 0 && y == 0);
        }
        
        private static bool Spell19 (int x, int y)
        {
            return new List<int> { x, y }.Any(e => e == 0 || e == 24);
        }
        
        private static bool Spell20 (int x, int y)
        {
            return (x % 2 == 0 && y % 2 == 0) || (x % 2 == 1 && y % 2 == 1);
        }
        
        private static bool Spell21 (int x, int y)
        {
            return y % (x + 1) == 0;
        }
        
        private static bool Spell22 (int x, int y)
        {
            return (y + x) % 3  == 0;
        }

        private static bool Spell23 (int x, int y)
        {
            return x % 3 == 0 && y % 2 == 0;
        }
        
        private static bool Spell24 (int x, int y)
        {
            return x == y || x + y == 24;
        }
        
        private static bool Spell25 (int x, int y)
        {
            return x % 6 == 0 || y % 6 == 0;
        }
    }
}